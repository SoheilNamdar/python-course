# *** TP 3 ***

# 1. Ecrire un script tp3-exo1.py qui produira un dossier flagsBis
# Le dossier flagsBis contiendra tous les fichiers png situés dans le dossier flags mais
# renommés en ne conservant que les deux premières lettres du nom d'origine.
# Ces lettres seront en majuscule.
# ex: 
#   flags/allemagne.png => flagsBis/AL.png
#   flags/belgique.png => flagsBis/BE.png
# Le fichier missing.png devra être ignoré
# En option: mettre en place une gestion d'erreur

import os

if not os.path.exists("flagsBis"):
    os.mkdir("flagsBis")
    print("[+] flagsBis dir created")


try:
    os.mkdir("flagsBis")
except FileExistsError:
    print("File existe deja !")


# déplacer les fichier png situés dans le dossier flags
for f in os.listdir("flags"):
    #os.remove("missing.png")
    if f.endswith(".png") and f != 'missing.png':
        os.rename("flags/" + f,"flagsBis/" + f[:2].upper()+".png")
        print("[+] files %s moved" % f)

print("*** The End ***")