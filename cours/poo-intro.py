class Student:
    # propriétés
    _age = 0
    first_name = ""
    last_name = ""

    # méthodes (fonctions dans une classe)
    def test(self):
        print("test")

    def get_age(self):
        return self._age

    def get_fist_name(self):
        return self.first_name

    # mutateur (setters)
    def set_age(self, age):
        self._age = age

# instanciation
s1 = Student()
s2 = Student()
# print(type(s1))

s1.first_name = "Toto"
#print(s1.first_name)

# print(s1.age)
print(s1.get_age())
#print(get_fist_name("soheil"))


s2.first_name = "Toto"
print(s2.first_name)

# invocation d'une method à partir d'un objet
s1.test()
s2.test()



# class Str:

#     def endwith(ch):
#         return 



s1.set_age(18)
print(s1.get_age())

s2.set_age(99)
print(s2.get_age())

class Vehicle:
    _num_wheels = 0
    _max_speed = 100

    def __init__(self, num_wheels, max_speed=100):
        #print("Hello !!!")
        self._num_wheels = num_wheels
        self._max_speed = max_speed


    def get_num_wheels(self):
        return self._num_wheels 

    def get_max_speed(self):
        return self._max_speed 

v1 = Vehicle(2)     
v2 = Vehicle(4,180)  

print(v1.get_num_wheels())
print("Vitesse max du vehicle v1:", v1.get_max_speed())
print("Vitesse max du vehicle v2:", v2.get_max_speed())