temperature = 2 #int
pi = 3.14 # float
isEarthRound = True #boolean
training = "initiation python" #str

# Affichage du type variables
# print(temperature)
# print(type(temperature))
# print(type(pi))
# print(type(training))

'''
print(2+2)
print(temperature + temperature)
training = "Perfectionnement Python"
print(training)
'''
"""
# Operation pour les variables
print(temperature + 10) # addition entier
print(training + " 10 ")
print(pi + 10)
print(isEarthRound + 10) #conversion implicite, True => 1
print("Le double pi est: " + str(pi*2)) # fonction de reconversion str()
doublePi = pi * 2 # stockage en variable du retour d'une expr. arithmétique
print(type(doublePi)) # float
doublePiStr = str(doublePi)
print(type(doublePiStr)) #string
"""

